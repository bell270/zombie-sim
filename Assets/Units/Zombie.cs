
using System;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    [SerializeField] private LayerMask _soundLayer;

    private float moveTimer;
    [SerializeField] private float frameTime = 1;
    [SerializeField] private float _maxVelocity = 20f;
    [SerializeField] private float _agility = 5;

    [SerializeField] private float _interest = 0f;
    [SerializeField] private float _interestDecrease = 0.05f;
    [SerializeField] private Vector3 _directionToInterest;

    private float roarTimer;
    [SerializeField] private GameObject SoundPrefab;
    [SerializeField] private float _stepSound = 0.1f;
    [SerializeField] private Vector2 _roarSound = new Vector2(0.05f,0.1f);
    private Vector2 roarRange = new Vector2(2, 20);
    [SerializeField] private bool _stepFlg = false;
    [SerializeField] private bool _roarFlg = false;

    [SerializeField] private Vector3 _color;

    [SerializeField] private int _damageDealt = 5;
    [SerializeField] private int _health = 100;

    private Animator _animator;

    private void Start()
    {
        _animator = gameObject.GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        if (_health < 0)
        {
            return;
        }
            
        SetLookDirection();

        /*LookForInterestPoint();*/
        moveTimer += Time.deltaTime;
        if (moveTimer >= frameTime)
        {
            Vector3 currentColor = _interest * new Vector3(1, 0, 0) + (1 - _interest) * _color;
            GetComponent<SpriteRenderer>().color = new Color(currentColor.x, currentColor.y, currentColor.z);
            ChangeVelocity();
            moveTimer = 0;
            if (_stepFlg)
            {
                EmitSound(GetComponent<Rigidbody2D>().velocity.magnitude * _stepSound, 0.3f);
            }
        }
        if (_roarFlg)
        {
            roarTimer += Time.deltaTime;
            if (roarTimer >= UnityEngine.Random.Range(roarRange.x, roarRange.y))
            {
                EmitSound(UnityEngine.Random.Range(_roarSound.x, _roarSound.y), 1f);
                roarTimer = 0;
            }
        }
    }

    private void FixedUpdate()
    {
        if (_health < 0)
        {
            return;
        }
    }

    private void EmitSound(float volume, float soundPower)
    {
        GameObject newSound = Instantiate(SoundPrefab, transform.position, Quaternion.identity);
        newSound.GetComponent<Sound>().name = "sound_" + gameObject.name;
        newSound.GetComponent<Sound>().SetMaxRadius(volume);
        newSound.GetComponent<Sound>().SetSoundPower(soundPower);
        newSound.GetComponent<Sound>().SetEmitter(gameObject.name);
        return;
    }

    private void LookForInterestPoint()
    {
        Decoy closestDecoy = null;
        Decoy[] decoys = FindObjectsOfType<Decoy>();
        foreach(Decoy decoy in decoys)
        {
            if(closestDecoy is null
                || Vector2.Distance(decoy.transform.position, gameObject.transform.position) < Vector2.Distance(closestDecoy.transform.position, gameObject.transform.position))
            {
                closestDecoy = decoy;
            }
        }
        if (closestDecoy != null)
        {
            _directionToInterest = (closestDecoy.transform.position - transform.position).normalized;
            _interest = 1;
        }
    }

    private void ChangeVelocity()
    {
        Vector2 velocityDelta;
        if (_interest <= 0f)
        {
            velocityDelta = _agility * new Vector2(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
        }
        else
        {
            velocityDelta = _agility * _directionToInterest;
            _interest -= _interestDecrease;
        }

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity += velocityDelta;
        if (rb.velocity.magnitude > _maxVelocity)
        {
            rb.velocity = rb.velocity.normalized * _maxVelocity;
        }
    }

    private void SetLookDirection()
    {
        Vector3 relativePos = GetComponent<Rigidbody2D>().velocity;
        Vector2 relativePosFlat = new Vector2(relativePos.x, relativePos.y);
        float rotateAngle = Vector2.SignedAngle(new Vector2(1, 0), relativePosFlat);
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotateAngle);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Plane plane = collision.collider.GetComponent<Plane>();
        if (plane != null)
        {
            Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
            if (collision.contacts[0].normal.y != 0)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x, -velocity.y);
            }
            if (collision.contacts[0].normal.x != 0)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity.x, velocity.y);
            }
        }

        Player player = collision.collider.GetComponent<Player>();
        if (player != null)
        {
            _animator.SetTrigger("PlayerCaughtTrigger");
            Vector3 setback = collision.GetContact(0).normal.normalized * 0.05f;
            transform.position += setback;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SoundWave sound = collision.GetComponent<SoundWave>();
        if (sound != null)
        {
            if (sound.GetComponent<SoundWave>().GetEmitter() != gameObject.name)
            {
                if (sound.GetSoundPower() >= _interest)
                {
                    _interest = sound.GetSoundPower();
                    _directionToInterest = (collision.transform.position - transform.position).normalized;
                }
            }
        }
    }
    
    public int getDamageDealt()
    {
        return _damageDealt;
    }
}
