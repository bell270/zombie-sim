using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private const string _actionMove = "move";
    private const string _actionAttack = "attack";
    private const string _actionScream = "scream";
    private const string _actionShout = "shout";
    private const string _actionOther = "other";
    private Vector3 _playerColor = new Vector3(0, 0, 1);

    [SerializeField] private int _health = 100;
    private const int _healthMax = 100;
    private bool _isAlive = true;
    [SerializeField] private Bar _healthBar;

    [SerializeField] private float _stamina = 100f;
    [SerializeField] private float _staminaCap = 100f;
    [SerializeField] private const float _staminaMax = 100f;
    [SerializeField] private float _staminaRefill = 0.5f;
    [SerializeField] private float _staminaPerStep = 1f;
    [SerializeField] private float _staminaPerAttack = 5f;
    [SerializeField] private Bar _staminaBar;

    private List<Decoy> decoyList = new List<Decoy>();
    [SerializeField] private GameObject DecoyPrefab;
    [SerializeField] private GameObject SoundPrefab;

    [SerializeField] private float _speed = 0.55f;
    [SerializeField] private float _runMultiplier = 1.75f;
    [SerializeField] private float _crouchMultiplier = 0.5f;
    [SerializeField] private float _stepsPerSecond = 1;
    private bool _runFlg = false;
    private bool _crouchFlg = false;
    private Rigidbody2D _rb;
    private Vector2 _moveInput;
    private Vector2 _moveVelocity;
    [SerializeField] private float _realSpeed;

    private float _stepVolume = 0.25f;
    private float _shoutVolume = 2f;
    private float _screamVolume = 0.5f;

    private float _fixedTimerMovement;
    private float _fixedTimerStamina;

    private Animator _animator;

    private void Start()
    {
        _animator = gameObject.GetComponent<Animator>();
        _rb = gameObject.GetComponent<Rigidbody2D>();
        _fixedTimerMovement = 0f;
        _fixedTimerStamina = 0f;
        _healthBar.SetBarColor(Color.red);
        _staminaBar.SetBarColor(Color.blue);
}

    private void Update()
    {
        CheckStatus();

        if (_isAlive)
        {
            SetLookDirection();
            SetMoveFlgs();
            _moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            _moveVelocity = _moveInput.normalized * _speed * (_runFlg ? _runMultiplier : 1) * (_crouchFlg ? _crouchMultiplier : 1);
            _realSpeed = _moveVelocity.magnitude;

            List<Decoy> decoyDelete = new List<Decoy>();
            if (decoyList.Count > 0)
            {
                foreach (Decoy decoy in decoyList)
                {
                    int decoyDied = decoy.AddLifeTime(Time.deltaTime);
                    if (decoyDied == 1)
                    {
                        decoyDelete.Add(decoy);
                    }
                }
                foreach (Decoy decoy in decoyDelete)
                {
                    decoyList.Remove(decoy);
                }
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                EmitSound(_shoutVolume, _actionShout);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                StaminaDecrease(_actionAttack);
            }

            if (Input.GetMouseButtonDown(1))
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GameObject newDecoy = Instantiate(DecoyPrefab, new Vector3(mousePos.x, mousePos.y, 0), Quaternion.identity);
                newDecoy.name = "decoy" + decoyList.Count;
                decoyList.Add(newDecoy.GetComponent<Decoy>());
                return;
            }
        }
        if (!_isAlive)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                EmitSound(_shoutVolume, _actionShout);
            }
        }
    }

    private void FixedUpdate()
    {
        if (_isAlive)
        {
            _fixedTimerMovement += Time.fixedDeltaTime;
            _fixedTimerStamina += Time.fixedDeltaTime;
            if (_fixedTimerMovement * _stepsPerSecond * (_runFlg ? _runMultiplier : 1) * (_crouchFlg ? _crouchMultiplier : 1) > 1f && _moveVelocity.magnitude > 0)
            {
                EmitSound(_moveVelocity.magnitude * _stepVolume, _actionMove);
                StaminaDecrease(_actionMove);
                _fixedTimerMovement = 0f;
            }
            if (_fixedTimerStamina > 1f && (_moveVelocity.magnitude == 0 || _crouchFlg))
            {
                StaminaAutoRefill();
                _fixedTimerStamina = 0f;
            }
            _rb.MovePosition(_rb.position + _moveVelocity * Time.fixedDeltaTime);
        }
    }


    private void SetLookDirection()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 targetPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector3 relativePos = targetPosition - transform.position;
        Vector2 relativePosFlat = new Vector2(relativePos.x, relativePos.y);
        float rotateAngle = Vector2.SignedAngle(new Vector2(1, 0), relativePosFlat);
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotateAngle);
    }

    private void CheckStatus()
    {
        _isAlive = _health > 0;
        Vector3 newColor = (_health * _playerColor + (_healthMax - _health) * (new Vector3(0.5f, 0.65f, 0.5f))) / _healthMax;
        GetComponent<SpriteRenderer>().color = new Color(newColor.x, newColor.y, newColor.z);
        _healthBar.SetBarValue((float)_health/_healthMax);
        _healthBar.SetBarText("" + _health + "/" + _healthMax);
        _staminaBar.SetBarValue(_stamina/_staminaMax);
        _staminaBar.SetBarBackValue(_staminaCap/ _staminaMax);
        _staminaBar.SetBarText("" + (int)_stamina + "/" + _staminaCap);
    }

    private void SetMoveFlgs()
    {
        bool staminaForWalk = _stamina > _staminaPerStep;
        bool staminaForRun = _stamina > _staminaPerStep * _runMultiplier;
        if (!staminaForWalk || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            _runFlg = false;
            _crouchFlg = true;
        }
        else if (staminaForRun && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            _runFlg = true;
            _crouchFlg = false;
        }
        else
        {
            _runFlg = false;
            _crouchFlg = false;
        }
    }

    private void StaminaAutoRefill()
    {
        _stamina = Math.Min(_staminaCap, _stamina + _staminaRefill);
    }

    private void StaminaDecrease(string actionType)
    {
        if (actionType.Equals(_actionMove))
        {
            _stamina -= (_runFlg ? _runMultiplier : 1) * (_crouchFlg ? 0 : 1) * _staminaPerStep;
        }
        if (actionType.Equals(_actionAttack) && (_stamina > _staminaPerAttack && _staminaCap > _staminaPerAttack))
        {
            _stamina -= 5;
            _staminaCap -= 5;
        }
    }

    private void StaminaIncrease(float staminaAddition)
    {
        _staminaCap += staminaAddition;
        _stamina = Math.Min(_staminaCap, _stamina + staminaAddition);
    }

    private void EmitSound(float volume, string soundType)
    {
        GameObject newSound = Instantiate(SoundPrefab, transform.position, Quaternion.identity);
        newSound.GetComponent<Sound>().name = "sound_" + gameObject.name + "_" + soundType;
        newSound.GetComponent<Sound>().SetSoundPower(volume);
        newSound.GetComponent<Sound>().SetEmitter(gameObject.name + "_" + soundType);
        return;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Zombie zombie = collision.collider.GetComponent<Zombie>();
        if (zombie != null)
        {
            ZombieCollision(zombie);
        }

        Wall wall = collision.collider.GetComponent<Wall>();
        if (wall != null)
        {
            _health = Math.Min(_health + 5, _healthMax);
            _stamina = Math.Min(_stamina + 5, _staminaMax);
            _staminaCap = Math.Min(_staminaCap + 5, _staminaMax);
            Vector3 setback = collision.GetContact(0).normal.normalized * 0.05f;
            transform.position += setback;
        }
    }

    private void ZombieCollision(Zombie zombie)
    {
        if (_isAlive)
        {
            _animator.SetTrigger("DamagedTrigger");
            _health = Math.Max(_health - zombie.getDamageDealt(), 0);
            EmitSound(_screamVolume, _actionScream);
        }
    }
}
