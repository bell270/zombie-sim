using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour
{
    private Image _background;
    private GameObject _bar;
    private GameObject _textbox;
    [SerializeField] Color _color = Color.grey;
    [SerializeField] float _value = 1f;
    [SerializeField] string _text = "placeholder";

    private void Start()
    {
        _background = GetComponent<Image>();
        _bar = GetComponentsInChildren<Image>()[1].gameObject;
        _textbox = GetComponentsInChildren<BarIndicator>()[0].gameObject;
        SetBarColor(_color);
        SetBarValue(_value);
        SetBarText(_text);
    }

    public void SetBarValue(float value)
    {
        _bar.GetComponent<Image>().fillAmount = value;
    }

    public float GetBarValue()
    {
        return _bar.GetComponent<Image>().fillAmount;
    }

    public void SetBarText(string text)
    {
        _textbox.GetComponent<BarIndicator>().SetText(text);
    }

    public string GetBarText()
    {
        return _textbox.GetComponent<BarIndicator>().GetComponent<Text>().text;
    }

    public void SetBarBackValue(float value)
    {
        _background.GetComponent<Image>().fillAmount = value;
    }

    public float GetBarBackValue()
    {
        return _background.GetComponent<Image>().fillAmount;
    }

    public void SetBarColor(Color newColor)
    {
        float whiteMultiplier = 3;
        Color newBarColor = new Color(newColor.r, newColor.g, newColor.b);
        Color.RGBToHSV(newBarColor, out float H, out float S, out float V);
        Color newBackColor = Color.HSVToRGB(H, S, 0.6f * V);
        _bar.GetComponent<Image>().color = newBarColor;
        _background.color = newBackColor;
    }
}
