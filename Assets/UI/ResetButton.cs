
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetButton : MonoBehaviour
{
    public void ResetScene()
    {
        Debug.Log("reset");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
