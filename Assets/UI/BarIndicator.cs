using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarIndicator : MonoBehaviour
{
    public Text textbox;
    void Start()
    {
        textbox = GetComponent<Text>();
    }

    public void SetText(string text)
    {
        textbox.text = text;
    }

}
