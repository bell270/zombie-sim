using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialize : MonoBehaviour
{
    [SerializeField] private int _zombiesAmount;
    [SerializeField] private GameObject _player;

    public GameObject ZombiePrefab;
    public GameObject PlayerPrefab;
    public GameObject DecoyPrefab;

    private List<GameObject> _zombies = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        CreateZombies();
    }

    // Update is called once per frame
    void Update()
    {
        CreateZombies();
    }

    private void CreateZombies()
    {
        Vector3 gameField = GameObject.Find("Plane").GetComponent<Renderer>().bounds.size / 2;
        while (_zombies.Count < _zombiesAmount)
        {
            Vector3 randPosition = new Vector3(Random.Range(-gameField.x, gameField.x), Random.Range(-gameField.y, gameField.y), 0);
            GameObject newZombie = Instantiate(ZombiePrefab, randPosition, Quaternion.identity) as GameObject;
            newZombie.name = "Zombie" + _zombies.Count;
            _zombies.Add(newZombie);
        }
        while (_zombies.Count > _zombiesAmount && _zombies.Count > 0)
        {
            Destroy(_zombies[_zombies.Count - 1]);
            _zombies.RemoveAt(_zombies.Count - 1);
        }
    }
}
