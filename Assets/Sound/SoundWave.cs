using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWave : MonoBehaviour
{
    private float _timerReset = 0.05f;
    private float _timer = 0f;
    private Vector2 _direction;
    [SerializeField] private float _decay;
    private float _decayThreshold;
    private float _currentPower = 0f;
    private float _currentDistance;
    [SerializeField] private float _distanceOverTime;
    [SerializeField] private int _segment;
    private string _emitter;
    private List<Vector2> _points = new List<Vector2>();
    [SerializeField] public bool _lineRendererFlg = true;

    void Start()
    {
        _currentDistance = 0;
        _decayThreshold = 0.2f;
    }

    void Update()
    { 
        _timer += Time.deltaTime;
        if (_timer >= _timerReset)
        {
            Tick();
            _timer = 0f;
        }
    }

    private void Tick()
    {
        if (_decayThreshold <= 0)
        {
            _currentPower -= _decay;
        }
        else
        {
            _decayThreshold -= _decay;
        }
        if (_currentPower > 0)
        {
            _currentDistance += _distanceOverTime;
            if (_lineRendererFlg)
            {
                CreateLine();
            }
            CreateCollider();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void CreateCollider()
    {
        EdgeCollider2D edge = gameObject.GetComponent<EdgeCollider2D>();
        edge.points = _points.ToArray();
    }

    private void CreateLine()
    {
        _points.Clear();

        LineRenderer line = gameObject.GetComponent<LineRenderer>();
        float angleDelta = 360f / (2 * _segment);
        float angle = Vector2.SignedAngle(new Vector2(1, 0), _direction);

        line.useWorldSpace = false;
        line.positionCount = 2;

        line.startColor = new Color(0.7f, 1, 1, _currentPower);
        line.endColor = new Color(0.7f, 1, 1, _currentPower);
        line.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
        line.startWidth = 0.1f;
        line.endWidth = 0.1f;

        float z = 0f;
        float x1 = Mathf.Sin(Mathf.Deg2Rad * (angle + angleDelta)) * _currentDistance;
        float y1 = Mathf.Cos(Mathf.Deg2Rad * (angle + angleDelta)) * _currentDistance;
        float x2 = Mathf.Sin(Mathf.Deg2Rad * (angle - angleDelta)) * _currentDistance;
        float y2 = Mathf.Cos(Mathf.Deg2Rad * (angle - angleDelta)) * _currentDistance;

        line.SetPosition(0, new Vector3(x1, y1, z));
        line.SetPosition(1, new Vector3(x2, y2, z));

        _points.Add(new Vector2(x1,y1));
        _points.Add(new Vector2(x2,y2));
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Wall wall = collision.GetComponent<Wall>();
        if (wall != null)
        {
            _currentPower -= wall.GetSoundResist() ;
            _decayThreshold = 0;
        }
    }

    public void SetDirection(Vector2 direction)
    {
        _direction = direction;
    }
    public Vector2 GetDirection()
    {
        return _direction;
    }

    public void SetSoundPower(float soundPower)
    {
        this._currentPower = soundPower;
    }
    public float GetSoundPower()
    {
        return _currentPower;
    }
    public void SetEmitter(string emitter)
    {
        _emitter = emitter;
    }
    public string GetEmitter()
    {
        return _emitter;
    }
    public void SetSegment(int segment)
    {
        _segment = segment;
    }
    public int GetSegment()
    {
        return _segment;
    }
}
