using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    private float _timerReset = 1f;
    private float _timer = 0f;
    [SerializeField] private int segmentsAmount = 12;
    private float _radius;
    [SerializeField] private float _radiusInc = 0.1f;
    private string _emitter;
    [SerializeField] private float maxRadius = 2;
    private float _soundPower;
    List<GameObject> lines = new List<GameObject>();
    [SerializeField] private bool _lineRendererFlg = true;
    [SerializeField] private GameObject SoundWavePrefab;

    void Start()
    {
        _timerReset = 1f;
        _timer = 0f;
        _radius = 0f;

        float angleDelta = 360f / segmentsAmount;
        for (int i = 0; i < segmentsAmount; i++)
        {
            GameObject newSound = Instantiate(SoundWavePrefab, transform.position, Quaternion.identity);
            newSound.name = gameObject.name + "_" + i;
            SoundWave newSoundWave = newSound.GetComponent<SoundWave>();
            newSoundWave.SetEmitter(_emitter);
            newSoundWave.SetSoundPower(_soundPower);
            newSoundWave.SetSegment(segmentsAmount);
            newSoundWave._lineRendererFlg = _lineRendererFlg;
            newSoundWave.SetDirection((new Vector2(Mathf.Sin(Mathf.Deg2Rad * i * angleDelta), Mathf.Cos(Mathf.Deg2Rad * i * angleDelta))).normalized);

            newSound.transform.SetParent(transform);
        }
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= _timerReset)
        {
            Tick();
            _timer = 0f;
        }
    }

    private void Tick()
    {
        if (gameObject.GetComponentsInChildren<Transform>().Length <= 1)
        {
            Destroy(gameObject);
            return;
        }
    }

    private void CreateLine()
    {
        foreach(GameObject line in lines)
        {
            Destroy(line);
        }
        lines.Clear();

        float angle = 0f;
        for (int i = 0; i < segmentsAmount; i++)
        {
            LineRenderer line = new GameObject().AddComponent<LineRenderer>();
            lines.Add(line.gameObject);
            line.useWorldSpace=false;
            line.positionCount = 2;

            line.startColor = new Color(0.7f, 1, 1, _soundPower);
            line.endColor = new Color(0.7f, 1, 1, _soundPower);
            line.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
            line.startWidth = 0.05f;
            line.endWidth = 0.05f;

            float z = 0f;
            float x1 = Mathf.Sin(Mathf.Deg2Rad * angle) * _radius;
            float y1 = Mathf.Cos(Mathf.Deg2Rad * angle) * _radius;
            float x2 = Mathf.Sin(Mathf.Deg2Rad * (angle + (360f / segmentsAmount))) * _radius;
            float y2 = Mathf.Cos(Mathf.Deg2Rad * (angle + (360f / segmentsAmount))) * _radius;

            line.SetPosition(0, new Vector3(x1, y1, z));
            line.SetPosition(1, new Vector3(x2, y2, z));

            angle += (360f / segmentsAmount);

            line.gameObject.transform.SetParent(transform);
            line.gameObject.transform.localPosition = Vector3.zero;
        }
    }

    public void SetMaxRadius(float radius)
    {
        maxRadius = radius;
    }
    public float GetMaxRadius()
    {
        return maxRadius;
    }
    public void SetSoundPower(float soundPower)
    {
        this._soundPower = soundPower;
    }
    public float GetSoundPower()
    {
        return _soundPower;
    }
    public void SetEmitter(string emitter)
    {
        _emitter = emitter;
    }
    public string GetEmitter()
    {
        return _emitter;
    }
}
