using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decoy : MonoBehaviour
{
    private float _lifeTime = 0f;
    [SerializeField] private float _ttl = 3;
    [SerializeField] private float _decoySoundVolume = 5f;
    [SerializeField] private int _decoyFrequency = 3;
    private int _signalsMade = 0;
    [SerializeField] private GameObject SoundPrefab;

    public int AddLifeTime(float inc)
    {
        _lifeTime += inc;
        if (_lifeTime >= (_signalsMade * _ttl/_decoyFrequency))
        {
            _signalsMade++;
            GameObject newSound = Instantiate(SoundPrefab, transform.position, Quaternion.identity);
            newSound.name = gameObject.name + "_sound";
            newSound.GetComponent<Sound>().SetEmitter(gameObject.name);
            newSound.GetComponent<Sound>().SetSoundPower(_decoySoundVolume);
        }
        Color fullColor = gameObject.GetComponent<SpriteRenderer>().color;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(fullColor.r, fullColor.g, fullColor.b, (_ttl-_lifeTime)/_ttl);
        if (_lifeTime > _ttl || _signalsMade >= _decoyFrequency)
        {
            Destroy(gameObject);
            return 1;
        }
        return 0;
    }
}
