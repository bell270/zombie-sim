using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    private Vector3 _initialPosition;
    [SerializeField] private float _soundResist = 1f;

    
    // Start is called before the first frame update
    void Start()
    {
        _initialPosition = transform.position;
        GetComponent<Rigidbody2D>().gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = _initialPosition;
    }

    internal float GetSoundResist()
    {
        return _soundResist;
    }
}
